fruit = 'banana'
x = len(fruit)
letter = fruit[x-1]
print(letter)

index = 0
while index < x :
    letter = fruit[index]
    print(index,letter)
    index = index + 1

for letter in fruit :
    print(letter)

count = 0
for letter in fruit :
    if letter == 'a' :
        count = count + 1
print(count)

# # Note that array slicing a[x:y] goes from a[x] to a[y-1],
# so that last slice number is "up to but not including"
s = 'Cacca rossa'
print(s[0:4])
print(s[3:8])
print(s[6:41000000]) # u can reference out iof length
print(s[2:])
print(s[:8])