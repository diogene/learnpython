# Chapter 10

# 10.1 - Tuples
# Tuples are a more efficeint version of unmodifieable lists
x = ('Glenn', 'Sally', 'Joseph')
print(x)
print(x[2])
print(max(x))

# Temporary vars are usually tuples becasue more efficient
t = tuple()
print(dir(t))

# tuple on left sign of assignment
(x, y) = (4, 'fred')
print(x,y)

# Tuple comparison is sequrntial in order
d = {'a':10, 'b':1, 'c':22}
print(sorted(d.items()))
print(d.items())

for k,v in sorted(d.items(), reverse=True) : # sort by keys
    print(k,v)

# Sorting by values
tmp = list()
for k,v in d.items() :
    tmp.append((v,k))
tmp = sorted(tmp, reverse=True)
print(tmp)

# Ten most common rwords
# fh = open('romeo.txt')
fh = open('mbox-short.txt')
counts = dict()
for line in fh :
    words = line.split()
    for word in words :
        counts[word] = counts.get(word,0) + 1

lst = list()
for k,v in counts.items() :
    lst.append((v, k))

lst = sorted(lst, reverse=True)

for v,k in lst[:10] :
    print(k, v)

# the last bit can be done also more swiftly with: this is the llist comprehension
c = {'a':10, 'b':1, 'c':22}
tmp = sorted( [ (v,k) for k,v in c.items() ], reverse=True )
print(tmp[:2])

# test
t = (2, 2, 300303)
# t[1] = 2
print(t.index(2))