fruit = 'banana'
if 'bn'in fruit :
    print('Found it')

# u can also sort strings (capital letter comes before small letters)
word = 'ruru'
if word > fruit :
    print(word,'is more than',fruit)

# sss
greet = '     Hello Bob     '
zap = greet.lower()
print(zap)
print(';HHHH'.lower())
type(greet)
dir(greet)
pos = fruit.find('na')
print(pos)
rep = greet.replace('Bob','jane')
print(rep)
print(greet.strip())
print(greet.lstrip())
print(greet.rstrip())

# example
data = 'From stephen.marquard@uct.ac.za  Sat 5 09:14:16 2008'
atpos = data.find('@')
print(atpos)
sppos = data.find('  ')
print(sppos)
host = data[atpos+1:sppos]
print(host)

# foreign
str = '入拉面'
print(type(str))

print('From marquard@uct.ac.za'[8])