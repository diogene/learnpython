# Chapter 9

# 9.1 - Dictionaries
# Dictionariue is another data structure -> a bag o value with its own label
purse = dict()
purse['money'] = 12 # key-value pairs
purse['candy'] = 3
purse['candy'] = purse['candy'] + 2
purse['tissues'] = list([75, 10])
print(purse)
print(purse['candy'])
print(purse['tissues'])

jjj = {'chuck': 12, 'fred': 42, 'jan': 100}
print(jjj)
print(jjj.__class__())

# 9.2 - Counting with Dictionaries
counts = dict()
names = ['csev', 'cwen', 'csev', 'zhen', 'zqian', 'cwen']
for name in names :
    if name not in counts :
        counts[name] = 1
    else :
        counts[name] = counts[name] + 1
print(counts)

# look at the key
x = counts.get('zhen',10000000)
print(x)
y = counts.get('ruru',10000000)
print(y)

# more clever
counts = dict()
names = ['csev', 'cwen', 'csev', 'zhen', 'zqian', 'cwen']
for name in names :
    counts[name] = counts.get(name,0) + 1
print(counts)

# 9.3 - Dictionaries and Files
counts = dict()
# line = input('Enter a line of text: ')
line = 'Oggi non son andato a fare la spesa'
words = line.split()
print('Words: ', words)
for word in words :
    counts[word] = counts.get(word,0) + 1
print('Counts', counts)

for key in counts :
    print(key, counts[key])

# Convert dictionary to a list
print(list(counts))
print(counts.keys())
print(counts.values())
print(counts.items())

# Loop with 2 iteration variables !!!!!
for key,value in counts.items() :
    print(key,value)

# exercise
name = 'words.txt'
name = 'clown.txt'
name = 'mbox-short.txt'
fh = open(name,'r')
counts = dict()
for line in fh :
    words = line.split()
    for word in words :
        counts[word] = counts.get(word,0) + 1
# print(counts)

bigcount = None
bigword = None
for word,count in counts.items() :
    if bigcount is None or count >= bigcount :
        bigword = word
        bigcount = count

print(bigword, bigcount)