# Watch out newlines is only 1 character (\n)
str = 'sette\n patate'
print(str.__len__())

# Manipulate files
handle = open('mbox.txt','r')
print(handle)
count = 0
for cheese in handle :
    count = count + 1
    print(cheese)
print('Line count:',count)

# Also usiong built-in funvtioon read
handle2 = open('test.txt','r')
inp = handle2.read()
print(inp)
print(inp.__len__())
print(inp[:20])

# More
handle3 = open('test.txt','r')
for line in handle3 :
    line = line.rstrip()
    if line.startswith('From:') :
        print(line)

# MOre
handle4 = open('mbox.txt','r')
for line in handle4 :
    line = line.rstrip()
    if not line.startswith('From:') :
        continue
    print(line)

# Deal with nn existing inlts
fname = input('Enter the file name:  ')
try :
    fhand = open(fname)
except :
    print('File cannot be opened: ',fname)
    quit()

count = 0
for line in fhand :
    if line.startswith('Subject:') :
        count = count + 1
print('There were',count,'subject lines in',fname)