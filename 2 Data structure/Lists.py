# Lists: in a single var we have a collectino of things

friends = ['Joseph', 'Glenn', 'Mary']
print(friends)

a = ['alpha', 1, 35.3838]
print(a)

for friend in friends :
    print('Merry XMas', friend)

# Strings are immutable
friends[0] = 'ss'
print(friends)

# Rannge function retrun a list of numbers taht range from 0 to one less than  input
print(range(len(friends)+50))
print(range(4))

for ii in range(len(friends)) :
    friend = friends[ii]
    print('Merry XMas', friend)

# 8.2 Manipuilatinf strings
a = [1, 2, 3]
b = [-1, -4, 100]
c = a + b # This does not sum, rather it concaenates
print(c)

a[0:]

# whole lot of methiods
stuff = list()
stuff.append('book')
stuff.append(99)
print(stuff)

friends.sort()
print(friends)

# accumulate
numlist = list([1, 3])
while False :
    inp = input('Enter a number:  ')
    if inp == 'done' : break
    value = float(inp)
    numlist.append(value)

average = sum(numlist)/len(numlist)
print('Average: ',average)

# 8.3 Lists and Strings - how are they wrokng together
abc = 'With three words'
stuff = abc.split()
print(stuff)

line = 'first;second;third'
thing = line.split(';')
thing1 = line.split() # SPlit looks to sl=plit in white spaces
print(thing)
print(thing1)

# handle
fhand = open('mbox-short.txt')
for line in fhand :
    line = line.rstrip()
    if not line.startswith('From ') : continue
    words = line.split()
    print(words[2])
print('Day of the week done.')

# douyble split
fhand = open('mbox-short.txt')
for line in fhand :
    line = line.rstrip()
    if not line.startswith('From ') : continue
    words = line.split()
    email = words[1]
    pieces = email.split('@')
    print(pieces[1])
print('Host is done.')
