# Assigment 13.2
# https://www.py4e.com/tools/python-data/?PHPSESSID=44765cea3374a301df2535bb2d8dc5ba

import urllib.request, urllib.parse, urllib.error, json, ssl

# url = 'http://py4e-data.dr-chuck.net/comments_42.json'
url = 'http://py4e-data.dr-chuck.net/comments_1069356.json'

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

print('Retrieving', url)
data = urllib.request.urlopen(url, context=ctx).read()
info = json.loads(data)
# print('User count:', len(info))
# print(info['comments'])
num = list()
for item in info['comments'] :
    # print(item)
    num.append(int(item['count']))

print('Count:', len(num))
print('Sum:', sum(num))