# Assignment 12.2
# https://www.py4e.com/tools/python-data/?PHPSESSID=8d01d3df87f7681bfb71b0ac378210c7

import urllib.request, urllib.parse, urllib.error, ssl
from bs4 import BeautifulSoup

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# url = 'http://py4e-data.dr-chuck.net/comments_42.html'
url = 'http://py4e-data.dr-chuck.net/comments_1069353.html'
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

# Retrieve all of the '<span' tags
tags = soup('span')
num = list()
for tag in tags:
    num.append(int(tag.contents[0]))
print(sum(num))