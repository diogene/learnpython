# Chapter 13

# 13.1 - Data on the Web
# In the wire we use the "wire format" and we need to use serialization
# format, the 2 most common exampls of serialziation code is XML and JSON

# 13.2 - Extensible Markup Language (XML)
# It is a serialization format language for exxhange data in the net (see test.xml)

# 13.3 - XML Schema
# Description of legacl contract and validating app
# Most used XML Schema are from W3C - (XSD)

# 13.4 - Parsing XML in Python
import xml.etree.ElementTree as et
data = '''<person>
    <name> Diogene </name>
    <phone type='intl'> +81 80 7707 5195 </phone>
    <email hide='yes' />
</person>'''
print(data)
tree = et.fromstring(data)
print(type(tree))
print('Name:', tree.find('name').text)
print('Attributes:', tree.find('email').get('hide'))
# print('Name:', tree.find('trono').text)

# List of trees while parsing XML (list within lists and trees within trees)
data = '''<stuff>
    <users>
        <user x='2'>
            <id> 001 </id>
            <name> Dr. Dei Tos </name>
        </user>
        <user x='7'>
            <id> 009 </id>
            <name> Mr. Marmo </name>
        </user>
    </users>
</stuff>'''
stuff = et.fromstring(data)
lst = stuff.findall('users/user')
print('User count:', len(lst))
for item in lst :
    print('Name: ', item.find('name').text)
    print('Id: ', item.find('id').text)
    print('Attributes: ', item.get('x'))
print('XML is a powerful serialization format tool! PANICOOO')

# 13.3 - JavaScript Object Notation (JSON)
# Increasngly popular as a simple tool for serialization (very native to  Java)
import json
data = '''{
    "name" : "Diogene",
    "phone" : {
        "type" : "intl",
        "number" : "+81 80 7707 5195"
    },
    "email" : {
        "hide" : "yes"
    }
}'''
info = json.loads(data) # This is a Pythion dictionary! It loads from string
print(info)
print('Name:', info['name'])
print('Hide:', info['email']['hide'])

data = '''[
    {
        "id" : "001",
        "x" : "2",
        "name" : "Dr. Dei Tos"
    },
    {
        "id" : "009",
        "x" : "7",
        "name" : "Mr. Marmo"
    }
]'''
info = json.loads(data)
print('User count:',len(info))
for item in info :
    print('Name:', item['name'])
    print('Id:', item['id'])
    print('Attribute:', item['x'])

# 13.6 - Using Application Programming Interfacs (API)
print('API oh yeah!!')
# http://maps.googleapis.com/maps/api/geocode/json?address=Ann+Harbor%2C+MI

import urllib.request, urllib.parse, urllib.error, json, ssl

api_key = False
# If you have a Google Places API key, enter it here
# api_key = 'AIzaSy___IDByT70'
# https://developers.google.com/maps/documentation/geocoding/intro

if api_key is False :
    api_key = 42
    serviceurl = 'http://py4e-data.dr-chuck.net/json?'
else :
    serviceurl = 'https://maps.googleapis.com/maps/api/geocode/json?'

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while True :
    address = input('Enter a location: ')
    if len(address) < 1 : break

    parms = dict()
    parms['address'] = address
    if api_key is not False: parms['key'] = api_key
    url = serviceurl + urllib.parse.urlencode(parms)

    print('Retrieving',url)
    uh = urllib.request.urlopen(url, context=ctx)
    data = uh.read().decode()
    print('Retrieved',len(data),'characters')

    try :
        js = json.loads(data)
    except :
        js = None
    
    if not js or 'status' not in js or js['status'] != 'OK' :
        print('=== Failure to retrieve ===')
        print(data)
        continue

    print('Done')
    print(json.dumps(js, indent=4))

    lat = js['results'][0]['geometry']['location']['lat']
    lng = js['results'][0]['geometry']['location']['lng']
    print('lat', lat, 'lng', lng)
    location = js['results'][0]['formatted_address']
    print(location)