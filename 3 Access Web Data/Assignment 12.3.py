# Assignment 12.3
# https://www.py4e.com/tools/python-data/?PHPSESSID=8d01d3df87f7681bfb71b0ac378210c7

import urllib.request, urllib.parse, urllib.error, ssl
from bs4 import BeautifulSoup

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# url = 'http://py4e-data.dr-chuck.net/known_by_Fikret.html'
url = 'http://py4e-data.dr-chuck.net/known_by_Zuzanna.html'

count = 0
l = 7
pos = 17
while count <= l :
    print('Retrieving: ', url)
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')
    tags = soup('a')
    url = tags[pos].get('href', None)
    count = count + 1
    
# for tag in tags:
#     # Look at the parts of a tag
#     print ('TAG:',tag)
#     print ('URL:',tag.get('href', None))
#     print ('Contents:',tag.contents[0])
#     print ('Attrs:',tag.attrs)