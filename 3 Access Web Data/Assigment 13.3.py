# Assigment 13.3
# https://www.py4e.com/tools/python-data/?PHPSESSID=3ba98c14552fa3bd4864c97554656b7a

import urllib.request, urllib.parse, urllib.error, ssl, json

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# Retrieve stuff
serviceurl = 'http://py4e-data.dr-chuck.net/json?'
while True :
    address = input('Enter a location: ')
    if len(address) < 1 : break

    parms = dict()
    parms['address'] = address
    parms['key'] = 42
    url = serviceurl + urllib.parse.urlencode(parms)

    print('Retrieving',url)
    uh = urllib.request.urlopen(url, context=ctx)
    data = uh.read().decode()
    print('Retrieved',len(data),'characters')

    try :
        js = json.loads(data)
    except :
        js = None
    
    if not js or 'status' not in js or js['status'] != 'OK' :
        print('=== Failure to retrieve ===')
        print(data)
        continue

    # print('Done')
    # print(json.dumps(js, indent=4))
    # print('=================================')
    a = js['results'][0]['place_id']
    print('Place id', a)