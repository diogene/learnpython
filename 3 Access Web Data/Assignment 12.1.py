# Assignment 12.1

import socket as so
mysock = so.socket(so.AF_INET, so.SOCK_STREAM)
mysock.connect(('data.pr4e.org', 80))
cmd = 'GET http://data.pr4e.org/intro-short.txt HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)

while True :
    data = mysock.recv(512)
    if len(data) < 1 :
        break
    print(data.decode())
    # print(data.decode(),end='')
    # print(data)
mysock.close()