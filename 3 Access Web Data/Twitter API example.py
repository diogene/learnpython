# Twitter API example
# Get the codes at https://developer.twitter.com/en/portal/projects/1333729482121773056/apps/19464879/keys

import urllib.request, urllib.parse, urllib.error, ssl, json
from twurl import augment

print('* Calling Twitter')
url =  'https://api.twitter.com/1.1/statuses/user_timeline.json'
parc = {'screen_name': 'diogene_gg', 'count': '20'}
url = augment(url,parc)
print(url)

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

connection = urllib.request.urlopen(url, context=ctx)
data = connection.read().decode()
print(data)

print('==================')
headers = dict(connection.getheaders())
print(headers)