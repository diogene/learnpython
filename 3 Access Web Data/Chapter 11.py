# Chapter 11


# 11.1 - Regular expression (regexp)
# Very powerful and quite cryptic
import re as re

fh = open('mbox-short.txt')
for line in fh :
    line = line.rstrip()
    if re.search('^From:', line) : # this equivalent to line.startwith('From:')
        print(line)
    if re.search('^X.*:', line) : # this equivalent to line.startwith('From:')
        print(line)
    if re.search('^X-\S+:', line) : # this equivalent to line.startwith('From:')
        print(line)

# 11.2 - Extracting Data
x = 'My 2 favorite numbers are 19 and 42'
y = re.findall('[0-9]+',x)
z = re.findall('[AEIOUMNSa]+',x)
print(y)
print(type(y))
print(z)

# Greedy mataching be carefukl
x = 'From: Using the : character'
y = re.findall('^F.+:', x)
print(y)

# FIx the greediness wth the ? character
y = re.findall('^F.+?:', x)
print(y)

# email finder
fh = open('mbox-short.txt')
for line in fh :
    y = re.findall('^From (\S+@\S+)',line) # use the parenthesito extarct only what I want
    # print(y)

# Dual split even more elegant to find host of email address
lin = 'From louis@media.berkeley.edu Sat Jan 5 09:10:16 2082'
y = re.findall('^From .*@([^ ]*)', lin)
print(y)

# Spam confidence
fh = open('mbox-short.txt')
numlist = list()
for line in fh :
    line = line.rstrip()
    stuff = re.findall('^X-DSPAM-Confidence: ([0-9.]+)', line)
    if len(stuff) != 1 : continue
    num = float(stuff[0])
    numlist.append(num)
print('Maximum:', max(numlist))

# Special character toj= just behave normally then u suse '\'
x = 'We juist received $1000.50 for holidays!'
y = re.findall('\$[0-9.]+', x)
print(y)


# 11.3 - 