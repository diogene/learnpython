# Assignment 11.1
# https://www.py4e.com/tools/python-data/?PHPSESSID=a4d0c520d42b6ade4656d802c7be4ea0

import re as re
# fh = open('regex_sum_42.txt', 'r')
fh = open('regex_sum_1069351.txt', 'r')
num = list()
for line in fh :
    line.rstrip()
    x = re.findall('[0-9]+', line)
    if len(x) == 0 : continue
    for word in x :
        num.append(int(word))

print(sum(num))