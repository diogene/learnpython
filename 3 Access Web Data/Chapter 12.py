# Chapter 12

# 12.1 - Networked technology
import socket as so
mysock = so.socket(so.AF_INET, so.SOCK_STREAM)
mysock.connect(('data.pr4e.org', 80))
print(mysock)

# 12.2 - Hypertext Transfer Protocol (HTTP)
cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)

while True :
    data = mysock.recv(512)
    if len(data) < 1 :
        break
    print(data.decode())
    # print(data)
mysock.close()

# 12.2a Using the Developer Console to Explore HTTP
# See safari aahahahah

# 12.3 Unicode Characters and Strings
print(ord('H')) # get the ASCII code of a character
# in pythion all str are unicode
x = b'abc' # byte string
y = u'abc' # unicode string
z = '囊'
z1 = u'囊'
print(x, 'x is',type(x))
print(y, 'y is',type(y))
print(z, 'z is',type(z))
print(z1, 'z1 is',type(z1))

# the ducntion socket.function() needs the type of data. so UTF-8 or ASCII or something weird

# 12.4 - Retrieving Web Pages
import urllib.request, urllib.parse, urllib.error
fh = urllib.request.urlopen('http://data.pr4e.org/romeo.txt')
for line in fh :
    print(line.decode().strip())

fh = urllib.request.urlopen('http://data.pr4e.org/romeo.txt')
counts = dict()
for line in fh :
    words = line.decode().strip()
    for word in words :
        counts[word] = counts.get(word,0) + 1
print(counts)

fh = urllib.request.urlopen('http://www.dr-chuck.com/page1.htm')
for line in fh :
    print(line.decode().strip())

# 12.5 - Parsing Web Pages
# package beautiful soup poarse thry messy html code (which can have syntax errors!!)
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
# url = input('Enter - ')
url = 'http://www.dr-chuck.com/page1.htm'
# url = 'http://www.dr-chuck.com/page2.htm'
url = 'https://pypi.org'
html = urllib.request.urlopen(url).read()
soup = BeautifulSoup(html, 'html.parser')
# print(soup)
# print(type(soup))
tags = soup('a')
for tag in tags :
    print(tag.get('href', None))

# Example
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = 'http://www.dr-chuck.com/'
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

# Retrieve all of the anchor tags
tags = soup('a')
for tag in tags:
    print(tag.get('href', None))
