# Assignment 13.1
# https://www.py4e.com/tools/python-data/?PHPSESSID=b37cfe2564ae4274131ddd1ae6145ed8

import urllib.request, urllib.parse, urllib.error, ssl
import xml.etree.ElementTree as ET

# url ='http://py4e-data.dr-chuck.net/comments_42.xml'
url = 'http://py4e-data.dr-chuck.net/comments_1069355.xml'

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

print('Retrieving', url)
xml = urllib.request.urlopen(url, context=ctx).read()
# print(xml.decode())
tree = ET.fromstring(xml)
# counts = tree.findall('.//count')
counts = tree.findall('comments/comment/count')
num = list()
for count in counts :
    num.append(int(count.text))
print('Count:', len(num))
print('Sum:', sum(num))