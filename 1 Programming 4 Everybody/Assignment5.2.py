# Assignment 5.2
# Write a program that repeatedly prompts a user for integer numbers until 
# the user enters 'done'. Once 'done' is entered, print out the largest and 
# smallest of the numbers. If the user enters anything other than a valid 
# number catch it with a try/except and put out an appropriate message and 
# ignore the number. Enter 7, 2, bob, 10, and 4 and match the output below.

smallest_so_far = None
largest_so_far = None
while True :
    # Input a value
    num = input('Enter a number: ')

    # Checks
    if num == 'done' :
        break
    try :
        sval = float(num)
    except :
        print('Invalid input')
        continue
    
    # Find max and min
    if smallest_so_far is None :
        smallest_so_far = sval
    elif sval < smallest_so_far :
        smallest_so_far = sval
    if largest_so_far is None :
        largest_so_far = sval
    elif sval > largest_so_far :
       largest_so_far = sval

# Print results
print('Maximum is',int(largest_so_far))
print('Minimum is',int(smallest_so_far))