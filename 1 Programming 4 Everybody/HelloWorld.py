
# Print stuff
print("hello world")


# Random math
x = 1
print(x)
x = x+1
print(x)


# Conditional
if x < 10:
    print('ciao')


# While loop
n = 5
while n > 0:
    print(n)
    n = n-1
print('ok')


# reserved words
