# Conditional statement
x = 15
if x < 10:
    print('Smaller')
else:
    print('Bigger')
if x >= 20:
    print('Bigger')
if x != 5:
    print('oh yehah')

print('Finis')

for ii in range(5) :
    print(ii)
    if ii < 2 :
        print('Smaller than 2')
    print('Done with i', ii)
print('All ok.')

# Complicated conditional sentence
x = 1
if x <2 :
    print('small')
elif x < 0 :
    print('medium')
else :
    print('large')
print('done')

# Traceback or try/except
astr = 'Hello Bob'
astr = '127621'
try:
    istr = int(astr)
except:
    istr = -1

print('First', istr)

# Try sample
rawstr = input('Enter a number: ')
try:
    ival = int(rawstr)
except:
    ival = -1

if ival > 0 :
    print('Nice work')
else :
    print('Not a number!!')
