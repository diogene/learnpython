
# Indfinte Loops
# n = 5
# while n > 0:
#     print(n)
#     n = n -1

# print('RURU')
# print(n)
        
# # while True:
# #     line = input('> ')
# #     if line == 'done':
# #         break
# #     print(line)
# # print('Done')

# while True:
#     line = input('> ')
#     if line[0] == '#':
#         continue
#     if line == 'done':
#         break
#     print(line)
# print('Done!')


# Definite loops
for ii in [5,4,3,2,1] :
    print(ii)
print('Blastoff!')

friends = ['John','Simone','Stef']
for friend in friends :
    print('Happy New Year', friend)
print('Done!')

# Smart loops - What are we trying to accomplish??
largest_so_far = -1
print('Before',largest_so_far)
numbers = [9, 41, 12, 3, 74, 15]
for number in numbers:
    if number > largest_so_far :
        largest_so_far = number
    print(largest_so_far,number)
print('After',largest_so_far)

# Loops idioms
count = 0
print('Beofre',count)
for thing in [9, 41, 12, 3, 74, 15]:
    count = count + thing # Compute running total
    print(count,thing)
print('After',count)

# Filtering in loops
found = False
for value in numbers :
    if value == 3 :
        found = True
        break
print(found)

# Smallest number
smallest_so_far = None
print('Before',smallest_so_far)
numbers = [9, 41, 12, 3, 74, 15]
for number in numbers:
    if smallest_so_far is None :
        smallest_so_far = value
    elif number < smallest_so_far :
        smallest_so_far = number
    print(smallest_so_far,number)
print('After',smallest_so_far)