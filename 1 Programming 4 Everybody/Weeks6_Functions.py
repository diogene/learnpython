def thing():
    print('Hello function')
    print('ruu')
    a = 3
    return a

def findmax(ss):
    big = max(ss)
    print(big)
    return big

def greet(lang):
    if lang == 'es':
        print('Hola')
    elif lang == 'fr':
        print('Bonjour')
    else:
        print('Hello')

def greet2(lang):
    if lang == 'es':
        return ('Hola')
    elif lang == 'fr':
        return ('Bonjour')
    else:
        return ('Hello')

def addtwo(a,b):
    added = a+b
    return added

# Call stuff
big = findmax('Ciao Bella Ruruw and Z-World')
greet('sdsa')
print(greet2('es'),'Abbey')